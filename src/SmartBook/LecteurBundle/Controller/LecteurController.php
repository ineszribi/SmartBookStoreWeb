<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LecteurController
 *
 * @author Hichem
 */
namespace SmartBook\LecteurBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
class LecteurController extends Controller {
   public function indexAction()
   {
     return $this->render("SmartBookLecteurBundle:lecteur:index.html.twig",array());
   }
}
